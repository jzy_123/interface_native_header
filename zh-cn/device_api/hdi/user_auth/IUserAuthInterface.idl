/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup HdfUserAuth
 * @{
 *
 * @brief 提供用户认证驱动的标准API接口。
 *
 * 用户认证驱动为用户认证服务提供统一的访问接口。获取用户认证驱动代理后，用户认证服务可以调用相关接口注册执行器，管理用户认证凭据，
 * 完成PIN码和生物特征认证。
 *
 * @since 3.2
 */

/**
 * @file IUserAuthInterface.idl
 *
 * @brief 声明用户认证驱动的API接口。接口可用于注册执行器，管理用户认证凭据，完成PIN码和生物特征认证。
 *
 * @since 3.2
 */

package ohos.hdi.user_auth.v1_0;

import ohos.hdi.user_auth.v1_0.UserAuthTypes;

/**
 * @brief 声明用户认证驱动的API接口。
 *
 * @since 3.2
 * @version 1.0
 */
interface IUserAuthInterface {
    /**
     * @brief 初始化用户认证驱动缓存信息，用于用户认证框架进程启动时初始化信息。
     *
     * @return 0 表示操作成功。
     * @return 非0 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    Init();
    /**
     * @brief 添加认证执行器来获取认证能力，用于各认证基础服务如口令认证服务等将认证能力对接到用户认证框架。
     *
     * @param info 执行器注册信息{@link ExecutorRegisterInfo}。
     * @param index 用户认证框架的执行器索引。
     * @param publicKey 用户认证框架公钥。
     * @param templateIds 该执行器已注册的模版ID列表。
     *
     * @return 0 表示操作成功。
     * @return 非0 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    AddExecutor([in] struct ExecutorRegisterInfo info, [out] unsigned long index,
        [out] unsigned char[] publicKey, [out] unsigned long[] templateIds);
    /**
     * @brief 删除执行器，用于清理失效的执行器信息。
     *
     * @param index 用户认证框架的执行器索引。
     *
     * @return 0 表示操作成功。
     * @return 非0 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    DeleteExecutor([in] unsigned long index);
    /**
     * @brief 开启一个认证凭据管理会话，用于在请求管理用户认证凭据前获取有效挑战值。
     *
     * @param userId 用户ID。
     * @param challenge 随机挑战值，用于生成用户身份认证令牌。
     *
     * @return 0 表示操作成功。
     * @return 非0 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    OpenSession([in] int userId, [out] unsigned char[] challenge);
    /**
     * @brief 关闭认证凭据管理会话，完成用户认证凭据管理请求处理后，调用该接口使原挑战值失效。
     *
     * @param userId 用户ID。
     *
     * @return 0 表示操作成功。
     * @return 非0 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    CloseSession([in] int userId);
    /**
     * @brief 开始注册用户认证凭据。当注册凭据类型为口令且该用户已经注册了口令凭据时，将会更新口令凭据。
     *
     * @param userId 用户ID。
     * @param authToken 用户口令认证令牌。
     * @param param 注册凭据参数{@link EnrollParam}。
     * @param info 调度信息{@link ScheduleInfo}。
     *
     * @return 0 表示操作成功。
     * @return 非0 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    BeginEnrollment([in] int userId, [in] unsigned char[] authToken, [in] struct EnrollParam param, 
        [out] struct ScheduleInfo info);
    /**
     * @brief 更新用户凭据注册结果，完成凭据注册。
     *
     * @param userId 用户ID。
     * @param scheduleResult 执行器签发的注册结果。
     * @param credentialId 凭据ID。
     * @param oldInfo 已经删除的凭据信息{@link CredentialInfo}。
     *
     * @return 0 表示操作成功。
     * @return 非0 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    UpdateEnrollmentResult([in] int userId, [in] unsigned char[] scheduleResult, [out] unsigned long credentialId,
        [out] struct CredentialInfo oldInfo);
    /**
     * @brief 取消注册请求。
     *
     * @param userId 用户ID。
     *
     * @return 0 表示操作成功。
     * @return 非0 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    CancelEnrollment([in] int userId);
    /**
     * @brief 删除用户凭据信息。
     *
     * @param userId 用户ID。
     * @param credentialId 凭据ID。
     * @param authToken 用户口令认证令牌。
     * @param info 删除的凭据信息{@link CredentialInfo}。
     *
     * @return 0 表示操作成功。
     * @return 非0 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    DeleteCredential([in] int userId, [in] unsigned long credentialId, [in] unsigned char[] authToken, 
        [out] struct CredentialInfo info);
    /**
     * @brief 查询用户凭据信息。
     *
     * @param userId 用户ID。
     * @param authType 凭据类型{@link AuthType}。
     * @param infos 凭据信息{@link CredentialInfo}。
     *
     * @return 0 表示操作成功。
     * @return 非0 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    GetCredential([in] int userId, [in] enum AuthType authType, [out] struct CredentialInfo[] infos);
    /**
     * @brief 查询用户安全信息。
     *
     * @param userId 用户ID。
     * @param secureUid 安全用户ID。
     * @param infos 注册信息{@link EnrolledInfo}。
     *
     * @return 0 表示操作成功。
     * @return 非0 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    GetSecureInfo([in] int userId, [out] unsigned long secureUid, [out] struct EnrolledInfo[] infos);
    /**
     * @brief 删除用户口令认证凭据，在用户IAM系统内删除该用户，该请求由用户触发。
     *
     * @param userId 用户ID。
     * @param authToken 用户口令认证令牌。
     * @param deletedInfos 删除的凭据信息{@link CredentialInfo}。
     *
     * @return 0 表示操作成功。
     * @return 非0 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    DeleteUser([in] int userId, [in] unsigned char[] authToken, [out] struct CredentialInfo[] deletedInfos);
    /**
     * @brief 强制删除用户，该请求由系统内管理用户的模块触发。
     *
     * @param userId 用户ID。
     * @param deletedInfos 删除的凭据信息{@link CredentialInfo}。
     *
     * @return 0 表示操作成功。
     * @return 非0 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    EnforceDeleteUser([in] int userId, [out] struct CredentialInfo[] deletedInfos);
    /**
     * @brief 开始认证用户，并生成认证方案。
     *
     * @param contextId 上下文索引。
     * @param param 认证方案{@link AuthSolution}。
     * @param scheduleInfos 调度信息{@link ScheduleInfo}。
     *
     * @return 0 表示操作成功。
     * @return 非0 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    BeginAuthentication([in] unsigned long contextId, [in] struct AuthSolution param,
        [out] struct ScheduleInfo[] scheduleInfos);
    /**
     * @brief 更新认证结果，评估认证方案的认证结果。
     *
     * @param contextId 上下文索引。
     * @param scheduleResult 执行器签发的认证结果。
     * @param info 认证结果信息{@link AuthResultInfo}。
     *
     * @return 0 表示操作成功。
     * @return 非0 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    UpdateAuthenticationResult([in] unsigned long contextId, [in] unsigned char[] scheduleResult,
        [out] struct AuthResultInfo info);
    /**
     * @brief 取消用户认证请求。
     *
     * @param contextId 上下文索引。
     *
     * @return 0 表示操作成功。
     * @return 非0 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    CancelAuthentication([in] unsigned long contextId);
    /**
     * @brief 开始用户身份识别，并生成识别方案。
     *
     * @param contextId 上下文索引。
     * @param authType 用户身份识别类型@{AuthType}。
     * @param scheduleInfo 调度信息{@link ScheduleInfo}。
     *
     * @return 0 表示操作成功。
     * @return 非0 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    BeginIdentification([in] unsigned long contextId, [in] enum AuthType authType, [in] byte[] challenge,
        [in] unsigned int executorId, [out] struct ScheduleInfo scheduleInfo);
    /**
     * @brief 更新用户身份识别结果，生成身份识别方案的结果
     *
     * @param contextId 上下文索引。
     * @param scheduleResult 执行器签发的用户身份识别结果。
     * @param info 用户身份识别结果{@link IdentifyResultInfo}。
     *
     * @return 0 表示操作成功。
     * @return 非0 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    UpdateIdentificationResult([in] unsigned long contextId, [in] unsigned char[] scheduleResult,
        [out] struct IdentifyResultInfo info);
    /**
     * @brief 取消用户身份识别请求。
     *
     * @param contextId 上下文索引。
     *
     * @return 0 表示操作成功。
     * @return 非0 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    CancelIdentification([in] unsigned long contextId);
    /**
     * @brief 获取当前认证类型的认证结果可信等级。
     *
     * @param userId 用户ID。
     * @param authType 认证类型{@link AuthType}。
     * @param authTrustLevel 认证结果可信等级。
     *
     * @return 0 表示操作成功。
     * @return 非0 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    GetAuthTrustLevel([in] int userId, [in] enum AuthType authType, [out] unsigned int authTrustLevel);
    /**
     * @brief 获取指定认证结果可信等级下有效的认证方式。
     *
     * @param userId 用户ID。
     * @param authTypes 用于筛选的认证方式列表{@link AuthType}。
     * @param authTrustLevel 认证结果可信等级。
     * @param validTypes 有效的认证方式列表{@link AuthType}。
     *
     * @return 0 表示操作成功。
     * @return 非0 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    GetValidSolution([in] int userId, [in] enum AuthType[] authTypes, [in] unsigned int authTrustLevel,
        [out] enum AuthType[] validTypes);
}
/** @} */