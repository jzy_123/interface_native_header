/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup USB
 * @{
 *
 * @brief 提供统一的USB驱动标准接口，实现USB驱动接入。
 *
 * 上层USB服务开发人员可以根据USB驱动模块提供的标准接口获取如下功能：打开/关闭设备，获取设备描述符，获取文件描述符，打开/关闭接口，批量读取/写入数据，
 * 设置/获取设备功能，绑定/解绑订阅者等。
 *
 * @since 3.2
 * @version 1.0
 */

/**
 * @file IUsbInterface.idl
 *
 * @brief 声明标准的USB驱动接口函数。
 *
 * @since 3.2
 * @version 1.0
 */

/**
 * @brief USB驱动接口的包路径。
 *
 * @since 3.2
 * @version 1.0
 */
package ohos.hdi.usb.v1_0;

import ohos.hdi.usb.v1_0.UsbTypes;
import ohos.hdi.usb.v1_0.IUsbdSubscriber;
import ohos.hdi.usb.v1_0.IUsbdBulkCallback;

/**
 * @brief 定义USB驱动基本的操作功能。
 *
 * 上层USB服务调用相关功能接口，可以打开/关闭设备，获取设备描述符，批量读取/写入数据等。
 */
interface IUsbInterface {

    /**
     * @brief 打开设备，建立连接。
     *
     * @param dev USB设备地址信息，详见{@link UsbDev}。
     *
     * @return 0 表示操作成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    OpenDevice([in] struct UsbDev dev);

    /**
     * @brief 关闭设备，释放与设备相关的所有系统资源。
     *
     * @param dev USB设备地址信息，详见{@link UsbDev}。
     *
     * @return 0 表示操作成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    CloseDevice([in] struct UsbDev dev);

    /**
     * @brief 获取设备描述符，设备描述符提供了关于设备、设备的配置以及任何设备所归属的类的信息。
     *
     * @param dev USB设备地址信息，详见{@link UsbDev}。
     * @param descriptor USB设备的描述符信息。
     *
     * @return 0 表示操作成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    GetDeviceDescriptor([in] struct UsbDev dev, [out] unsigned char[] descriptor);

    /**
     * @brief 根据设备的字符串ID获取字符串描述符，字符串描述符是提供一些设备接口相关的描述性信息，比如厂商的名字、产品序列号等。
     *
     * @param dev USB设备地址信息，详见{@link UsbDev}。
     * @param descId USB设备的描述符ID。
     * @param descriptor 获取USB设备的字符串描述符。
     *
     * @return 0 表示操作成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    GetStringDescriptor([in] struct UsbDev dev, [in] unsigned char descId, [out] unsigned char[] descriptor);

    /**
     * @brief 根据设备的配置ID获取配置描述符，配置描述符包含有关配置及其接口、备用设置及其终结点的信息。
     *
     * @param dev USB设备地址信息，详见{@link UsbDev}。
     * @param descId USB设备的配置ID。
     * @param descriptor 获取USB设备配置信息。
     *
     * @return 0 表示操作成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    GetConfigDescriptor([in] struct UsbDev dev, [in] unsigned char descId, [out] unsigned char[] descriptor);

    /**
     * @brief 获取USB设备的原始描述符。
     *
     * @param dev USB设备地址信息，详见{@link UsbDev}。
     * @param descriptor USB设备的原始描述符。
     *
     * @return 0 表示操作成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    GetRawDescriptor([in] struct UsbDev dev, [out] unsigned char[] descriptor);

    /**
     * @brief 获取USB设备的文件描述符。
     *
     * @param dev USB设备地址信息，详见{@link UsbDev}。
     * @param fd USB设备的文件描述符。
     *
     * @return 0 表示操作成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    GetFileDescriptor([in] struct UsbDev dev, [out] int fd);

    /**
     * @brief 设置USB设备当前的配置信息，USB设备被主机配置过后，主机可以使用设备提供的所有功能。
     *
     * @param dev USB设备地址信息，详见{@link UsbDev}。
     * @param configIndex USB设备配置信息的字符串描述符索引值（数字字符串）。
     *
     * @return 0 表示操作成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    SetConfig([in] struct UsbDev dev, [in] unsigned char configIndex);

    /**
     * @brief 获取USB设备当前的配置信息。
     *
     * @param dev USB设备地址信息，详见{@link UsbDev}。
     * @param configIndex USB设备配置信息的字符串描述符索引值（数字字符串）。
     *
     * @return 0 表示操作成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    GetConfig([in] struct UsbDev dev, [out] unsigned char configIndex);

    /**
     * @brief 打开USB设备的接口并声明独占，必须在数据传输前执行。
     *
     * @param dev USB设备地址信息，详见{@link UsbDev}。
     * @param interfaceid USB设备接口ID。
     * @param force 是否强制，1表示强制，0表示不强制。
     *
     * @return 0 表示操作成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    ClaimInterface([in] struct UsbDev dev, [in] unsigned char interfaceid, [in] unsigned char force);

    /**
     * @brief 在停止数据传输后关闭占用的USB设备接口，并释放相关资源。
     *
     * @param dev USB设备地址信息，详见{@link UsbDev}。
     * @param interfaceid USB设备接口ID。
     *
     * @return 0 表示操作成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    ReleaseInterface([in] struct UsbDev dev, [in] unsigned char interfaceid);

    /**
     * @brief 设置USB设备指定接口的备选设置，用于在具有相同ID但不同备用设置的两个接口之间进行选择。
     *
     * @param dev USB设备地址信息，详见{@link UsbDev}。
     * @param interfaceid USB设备接口ID。
     * @param altIndex USB设备接口的备用设置信息。
     *
     * @return 0 表示操作成功。
     * @return 非零值 表示操作失败。
     * 
     * @since 3.2
     * @version 1.0
     */
    SetInterface([in] struct UsbDev dev, [in] unsigned char interfaceid, [in] unsigned char altIndex);

    /**
     * @brief 在USB设备指定端点方向为读取时，执行批量数据读取。
     *
     * @param dev USB设备地址信息，详见{@link UsbDev}。
     * @param pipe USB设备管道信息，详见{@link UsbPipe}。
     * @param timeout 超时时间。
     * @param data 读取的数据。
     *
     * @return 0 表示操作成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    BulkTransferRead([in] struct UsbDev dev, [in] struct UsbPipe pipe, [in] int timeout, [out] unsigned char[] data);

    /**
     * @brief 在USB设备指定端点方向为写入时，执行批量数据写入。
     *
     * @param dev USB设备地址信息，详见{@link UsbDev}。
     * @param pipe USB设备管道信息，详见{@link UsbPipe}。
     * @param timeout 超时时间。
     * @param data 写入的数据。
     *
     * @return 0 表示操作成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    BulkTransferWrite([in] struct UsbDev dev, [in] struct UsbPipe pipe, [in] int timeout, [in] unsigned char[] data);

    /**
     * @brief 在传输状态为读取并且控制端点是端点零时，对USB设备执行控制传输。
     *
     * @param dev USB设备地址信息，详见{@link UsbDev}。
     * @param ctrl USB设备控制数据，详见{@link UsbCtrlTransfer}。
     * @param data 读取的数据。
     *
     * @return 0 表示成功。
     * @return 非零值 表示失败。
     *
     * @since 3.2
     * @version 1.0
     */
    ControlTransferRead([in] struct UsbDev dev, [in] struct UsbCtrlTransfer ctrl, [out] unsigned char[] data);

    /**
     * @brief 在传输状态为写入并且控制端点是端点零时，对USB设备执行控制传输。
     *
     * @param dev USB设备地址信息，详见{@link UsbDev}。
     * @param ctrl USB设备控制数据，详见{@link UsbCtrlTransfer}。
     * @param data 写入的数据。
     *
     * @return 0 表示成功。
     * @return 非零值 表示失败。
     *
     * @since 3.2
     * @version 1.0
     */
    ControlTransferWrite([in] struct UsbDev dev, [in] struct UsbCtrlTransfer ctrl, [in] unsigned char[] data);

    /**
     * @brief 在USB设备指定端点方向为数据读取时执行中断数据读取。
     *
     * @param dev USB设备地址信息，详见{@link UsbDev}。
     * @param pipe USB设备管道信息，详见{@link UsbPipe}。
     * @param timeout 超时时间。
     * @param data 读取的数据。
     *
     * @return 0 表示操作成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    InterruptTransferRead([in] struct UsbDev dev, [in] struct UsbPipe pipe, [in] int timeout, [out] unsigned char[] data);

    /**
     * @brief 在USB设备指定端点方向为写入时执行中断数据写入。
     *
     * @param dev USB设备地址信息，详见{@link UsbDev}。
     * @param pipe USB设备管道信息，详见{@link UsbPipe}。
     * @param timeout 超时时间。
     * @param data 写入的数据。
     *
     * @return 0 表示操作成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    InterruptTransferWrite([in] struct UsbDev dev, [in] struct UsbPipe pipe, [in] int timeout, [in] unsigned char[] data);

    /**
     * @brief 在USB设备指定端点方向为读取时执行等时数据读取。
     *
     * @param dev USB设备地址信息，详见{@link UsbDev}。
     * @param pipe USB设备管道信息，详见{@link UsbPipe}。
     * @param timeout 超时时间。
     * @param data 读取的数据。
     *
     * @return 0 表示操作成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    IsoTransferRead([in] struct UsbDev dev, [in] struct UsbPipe pipe, [in] int timeout, [out] unsigned char[] data);

    /**
     * @brief 在USB设备指定端点方向为写入时执行等时数据写入。
     *
     * @param dev USB设备地址信息，详见{@link UsbDev}。
     * @param pipe USB设备管道信息，详见{@link UsbPipe}。
     * @param timeout 超时时间。
     * @param data 写入的数据。
     *
     * @return 0 表示操作成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    IsoTransferWrite([in] struct UsbDev dev, [in] struct UsbPipe pipe, [in] int timeout, [in] unsigned char[] data);

    /**
     * @brief 在USB设备指定端点上进行异步数据发送或者接收请求，数据传输方向由端点方向决定。
     *
     * @param dev USB设备地址信息，详见{@link UsbDev}。
     * @param pipe USB设备管道信息，详见{@link UsbPipe}。
     * @param clientData 用户数据。
     * @param buffer 传输的数据。
     *
     * @return 0 表示操作成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    RequestQueue([in] struct UsbDev dev, [in] struct UsbPipe pipe, [in] unsigned char[] clientData, [in] unsigned char[] buffer);

    /**
     * @brief 等待RequestQueue异步请求的操作结果。
     *
     * @param dev USB设备地址信息，详见{@link UsbDev}。
     * @param clientData 用户数据。
     * @param buffer 传输的数据。
     * @param timeout 超时时间。
     *
     * @return 0 表示操作成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    RequestWait([in] struct UsbDev dev, [out] unsigned char[] clientData, [out] unsigned char[] buffer, [in] int timeout);

    /**
     * @brief 取消待处理的数据请求。
     *
     * @param dev USB设备地址信息，详见{@link UsbDev}。
     * @param pipe USB设备管道信息，详见{@link UsbPipe}。
     *
     * @return 0 表示操作成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    RequestCancel([in] struct UsbDev dev, [in] struct UsbPipe pipe);

    /**
     * @brief 获取USB设备当前的功能（按位域表示）。
     *
     * @param funcs 设备当前的功能值。
     *
     * @return 0 表示操作成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    GetCurrentFunctions([out] int funcs);

    /**
     * @brief 设置USB设备当前的功能（按位域表示）。
     *
     * @param funcs 待设置的设备功能值。
     *
     * @return 0 表示操作成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    SetCurrentFunctions([in] int funcs);

    /**
     * @brief 设置USB设备端口的角色。
     *
     * @param portId USB设备端口ID。
     * @param powerRole 电源角色的值。
     * @param dataRole 数据角色的值。
     *
     * @return 0 表示操作成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    SetPortRole([in] int portId, [in] int powerRole, [in] int dataRole);

    /**
     * @brief 查询USB设备端口的当前设置信息。
     *
     * @param portId USB设备端口ID。
     * @param powerRole USB设备电源角色。
     * @param dataRole USB设备数据角色。
     * @param mode USB设备模式。
     *
     * @return 0 表示操作成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    QueryPort([out] int portId, [out] int powerRole, [out] int dataRole, [out] int mode);

    /**
     * @brief 绑定订阅者。
     *
     * @param subscriber 订阅者信息，详见{@link IUsbdSubscriber}。
     *
     * @return 0 表示操作成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    BindUsbdSubscriber([in] IUsbdSubscriber subscriber);

    /**
     * @brief 解绑订阅者。
     *
     * @param subscriber 订阅者信息，详见{@link IUsbdSubscriber}。
     *
     * @return 0 表示操作成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    UnbindUsbdSubscriber([in] IUsbdSubscriber subscriber);

    /**
     * @brief 注册批量传输异步回调函数。
     *
     * @param dev USB设备地址信息，详见{@link UsbDev}。
     * @param pipe USB设备管道信息，详见{@link UsbPipe}。
     * @param cb 回调函数对象，详见{@link IUsbdBulkCallback}。
     *
     * @return 0 表示操作成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    RegBulkCallback([in] struct UsbDev dev, [in] struct UsbPipe pipe, [in] IUsbdBulkCallback cb);

    /**
     * @brief 注销批量传输异步回调函数。
     *
     * @param dev USB设备地址信息，详见{@link UsbDev}。
     * @param pipe USB设备管道信息，详见{@link UsbPipe}。
     *
     * @return 0 表示操作成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    UnRegBulkCallback([in] struct UsbDev dev, [in] struct UsbPipe pipe);

    /**
     * @brief 批量传输异步读数据。
     *
     * @param dev USB设备地址信息，详见{@link UsbDev}。
     * @param pipe USB设备管道信息，详见{@link UsbPipe}。
     * @param ashmem 共享内存，用于存放读取的数据。
     *
     * @return 0 表示操作成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    BulkRead([in] struct UsbDev dev, [in] struct UsbPipe pipe, [in] Ashmem ashmem);

    /**
     * @brief 批量传输异步写数据。
     *
     * @param dev USB设备地址信息，详见{@link UsbDev}。
     * @param pipe USB设备管道信息，详见{@link UsbPipe}。
     * @param ashmem 为共享内存，用于存放需要写入的数据。
     *
     * @return 0 表示操作成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    BulkWrite([in] struct UsbDev dev, [in] struct UsbPipe pipe, [in] Ashmem ashmem);

    /**
     * @brief 批量传输异步取消接口，用于取消当前接口的异步批量读写操作。
     *
     * @param dev USB设备地址信息，详见{@link UsbDev}。
     * @param pipe USB设备管道信息，详见{@link UsbPipe}。
     *
     * @return 0 表示操作成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    BulkCancel([in] struct UsbDev dev, [in] struct UsbPipe pipe);
}
/** @} */
