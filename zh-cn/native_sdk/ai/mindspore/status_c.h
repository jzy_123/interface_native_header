/**
 * Copyright 2021 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef MINDSPORE_INCLUDE_C_API_STATUS_C_H
#define MINDSPORE_INCLUDE_C_API_STATUS_C_H

#include <stdint.h>

#ifdef __cplusplus
extern "C"
{
#endif
  /**
   * @brief Minspore不同组件的代码
   */
  enum OH_AI_CompCode
  {
    /** Minspore Core的代码 */
    OH_AI_COMPCODE_CORE = 0x00000000u,
    /** Minspore Lite的代码 */
    OH_AI_COMPCODE_LITE = 0xF0000000u,
  };

  /**
   * @brief Minspore的状态码
   */
  typedef enum OH_AI_Status
  {
    /** 通用的成功状态码 */
    OH_AI_STATUS_SUCCESS = 0,

    // Core
    /** Mindspore Core 失败状态码 */
    OH_AI_STATUS_CORE_FAILED = OH_AI_COMPCODE_CORE | 0x1,

    // Lite
    /** Mindspore Lite 异常状态码 */
    OH_AI_STATUS_LITE_ERROR = OH_AI_COMPCODE_LITE | (0x0FFFFFFF & -1),
    /** Mindspore Lite 空指针状态码 */
    OH_AI_STATUS_LITE_NULLPTR = OH_AI_COMPCODE_LITE | (0x0FFFFFFF & -2),
    /** Mindspore Lite 参数异常状态码 */
    OH_AI_STATUS_LITE_PARAM_INVALID = OH_AI_COMPCODE_LITE | (0x0FFFFFFF & -3),
    /** Mindspore Lite 未改变状态码 */
    OH_AI_STATUS_LITE_NO_CHANGE = OH_AI_COMPCODE_LITE | (0x0FFFFFFF & -4),
    /** Mindspore Lite 没有错误但是退出的状态码 */
    OH_AI_STATUS_LITE_SUCCESS_EXIT = OH_AI_COMPCODE_LITE | (0x0FFFFFFF & -5),
    /** Mindspore Lite 内存分配失败的状态码 */
    OH_AI_STATUS_LITE_MEMORY_FAILED = OH_AI_COMPCODE_LITE | (0x0FFFFFFF & -6),
    /** Mindspore Lite 功能未支持的状态码 */
    OH_AI_STATUS_LITE_NOT_SUPPORT = OH_AI_COMPCODE_LITE | (0x0FFFFFFF & -7),
    /** Mindspore Lite 线程池异常状态码 */
    OH_AI_STATUS_LITE_THREADPOOL_ERROR = OH_AI_COMPCODE_LITE | (0x0FFFFFFF & -8),
    /** Mindspore Lite 未初始化状态码 */
    OH_AI_STATUS_LITE_UNINITIALIZED_OBJ = OH_AI_COMPCODE_LITE | (0x0FFFFFFF & -9),

    // 执行器相关的错误码, 范围 [-100,-200)
    /** Mindspore Lite 张量溢出错误的状态码 */
    OH_AI_STATUS_LITE_OUT_OF_TENSOR_RANGE = OH_AI_COMPCODE_LITE | (0x0FFFFFFF & -100),
    /** Mindspore Lite 输入张量异常的状态码*/
    OH_AI_STATUS_LITE_INPUT_TENSOR_ERROR =
        OH_AI_COMPCODE_LITE | (0x0FFFFFFF & -101),
    /** Mindspore Lite 重入异常的状态码*/
    OH_AI_STATUS_LITE_REENTRANT_ERROR = OH_AI_COMPCODE_LITE | (0x0FFFFFFF & -102),

    // 图相关的错误码, 范围 [-200,-300)
    /** Mindspore Lite 文件异常状态码*/
    OH_AI_STATUS_LITE_GRAPH_FILE_ERROR = OH_AI_COMPCODE_LITE | (0x0FFFFFFF & -200),

    // 算子相关的错误码, 范围 [-300,-400)
    /** Mindspore Lite 未找到算子的状态码*/
    OH_AI_STATUS_LITE_NOT_FIND_OP = OH_AI_COMPCODE_LITE | (0x0FFFFFFF & -300),
    /** Mindspore Lite 无效算子状态码*/
    OH_AI_STATUS_LITE_INVALID_OP_NAME = OH_AI_COMPCODE_LITE | (0x0FFFFFFF & -301),
    /** Mindspore Lite 无效算子超参数状态码*/
    OH_AI_STATUS_LITE_INVALID_OP_ATTR = OH_AI_COMPCODE_LITE | (0x0FFFFFFF & -302),
    /** Mindspore Lite 算子执行失败的状态码*/
    OH_AI_STATUS_LITE_OP_EXECUTE_FAILURE =
        OH_AI_COMPCODE_LITE | (0x0FFFFFFF & -303),

    // 张量相关的错误码, 范围 [-400,-500)
    /** Mindspore Lite 张量格式异常状态码*/
    OH_AI_STATUS_LITE_FORMAT_ERROR = OH_AI_COMPCODE_LITE | (0x0FFFFFFF & -400),

    // 形状推理相关的错误码, 范围 [-500,-600)
    /** Mindspore Lite 形状推理异常状态码*/
    OH_AI_STATUS_LITE_INFER_ERROR = OH_AI_COMPCODE_LITE | (0x0FFFFFFF & -500),
    /** Mindspore Lite 无效的形状推理的状态码*/
    OH_AI_STATUS_LITE_INFER_INVALID =
        OH_AI_COMPCODE_LITE | (0x0FFFFFFF & -501),

    // 用户输入相关的错误码, 范围 [-600, 700)
    /** Mindspore Lite 用户输入的参数无效状态码*/
    OH_AI_STATUS_LITE_INPUT_PARAM_INVALID =
        OH_AI_COMPCODE_LITE | (0x0FFFFFFF & -600),

  } OH_AI_Status;
#ifdef __cplusplus
}
#endif
#endif // MINDSPORE_INCLUDE_C_API_STATUS_C_H
