/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup WLAN
 * @{
 *
 * @brief WLAN模块向上层WLAN服务提供了统一接口。
 *
 * 上层WLAN服务开发人员可根据WLAN模块提供的向上统一接口获取如下能力：建立/关闭WLAN热点，扫描/关联WLAN热点，WLAN平台芯片管理，网络数据缓冲的申请、释放、移动等操作，网络设备管理，电源管理等。
 *
 * @since 3.2
 * @version 1.0
 */

 /**
 * @file WlanTypes.idl
 *
 * @brief WLAN模块相关的数据类型。
 *
 * WLAN模块中使用的数据类型，包括feature对象信息、STA信息、扫描信息、网络设备信息等。
 *
 * @since 3.2
 * @version 1.0
 */

/**
 * @brief WLAN模块接口的包路径。
 *
 * @since 3.2
 * @version 1.0
 */
package ohos.hdi.wlan.v1_0;

/**
 * @brief feature对象信息。
 *
 * @since 3.2
 * @version 1.0
 */
struct HdfFeatureInfo {
    /** feature对象的网卡名称。 */
    String ifName;
    /** feature对象的类型。 */
    int type;
};

/**
 * @brief STA的信息。
 *
 * @since 3.2
 * @version 1.0
 */
struct HdfStaInfo {
    /** STA的MAC地址。 */
    unsigned char[] mac;
};

/**
 * @brief WiFi扫描参数SSID信息。
 *
 * @since 3.2
 * @version 1.0
 */
struct HdfWifiDriverScanSsid {
    /** WiFi扫描的SSID。 */
    String ssid;
    /** WiFi扫描的SSID长度。 */ 
    int ssidLen;
};

/**
 * @brief WiFi扫描参数。
 *
 * @since 3.2
 * @version 1.0
 */
struct HdfWifiScan{
    /** WiFi扫描的SSID集合。 */
    struct HdfWifiDriverScanSsid[] ssids;
    /** WiFi扫描的频率集合。 */
    int[] freqs;
    /** WiFi扫描请求中携带的扩展IE。 */
    unsigned char[] extraIes;
    /** WiFi扫描的BSSID。 */
    unsigned char[] bssid;
    /** SSID扫描的前缀标志。 */
    unsigned char prefixSsidScanFlag;
    /** 快速连接标志。 */
    unsigned char fastConnectFlag;
};

/**
 * @brief 网络设备信息。
 *
 * @since 3.2
 * @version 1.0
 */
struct HdfNetDeviceInfo {
    /** 网络设备索引。 */
    unsigned int index;
    /** 网卡名称。 */
    String ifName;
    /** 网卡名称长度。 */
    unsigned int ifNameLen;
    /** 网卡类型。 */
    unsigned char iftype;
    /** 网络设备MAC地址。 */
    unsigned char[] mac;
};

/**
 * @brief 网络设备信息集合。
 *
 * @since 3.2
 * @version 1.0
 */
struct HdfNetDeviceInfoResult {
    /** 网络设备信息集合。 */
    struct HdfNetDeviceInfo[] deviceInfos;
};

/**
 * @brief WiFi扫描结果。
 *
 * @since 3.2
 * @version 1.0
 */
struct HdfWifiScanResult {
    /** BSS/IBSS的标志位。 */
    int flags;
    /** BSSID信息。 */
    unsigned char[] bssid;
    /** Capability信息字段（主机字节序排列）。 */
    short caps;
    /** 信道频率。 */
    int freq;
    /** Beacon帧间隔。 */
    short beaconInt;
    /** 信号质量。 */
    int qual;
    /** 信号强度。 */
    int level;
    /** 收到最新的Beacon或者探测响应帧数据的时间长度，单位为毫秒。 */
    unsigned int age;
    /** 扫描结果中的变量值。 */
    unsigned char[] variable;
    /** 紧跟的Probe Response中IE字段。 */
    unsigned char[] ie;
    /** 紧跟的Beacon中IE字段。 */
    unsigned char[] beaconIe;
};

/**
 * @brief WiFi频段信息。
 *
 * @since 3.2
 * @version 1.0
 */
struct HdfWifiInfo {
    /** WiFi频段。 */
    int band;
    /** WiFi频段下支持的频率个数。 */
    unsigned int size;
};

/**
 * @brief 信道测量参数。
 *
 * @since 3.2
 * @version 1.0
 */
struct MeasChannelParam {
    /** 信道号。 */
    int channelId;
    /** 测量时间。 */
    int measTime;
};

/**
 * @brief 信道测量结果。
 *
 * @since 3.2
 * @version 1.0
 */
struct MeasChannelResult {
    /** 信道号。 */
    int channelId;
    /** 信道负载。 */
    int chload;
    /** 信道噪声。 */
    int noise;
};

/**
 * @brief 投屏参数。
 *
 * @since 3.2
 * @version 1.0
 */
struct ProjectionScreenCmdParam {
    /** 投屏命令ID。 */
    int cmdId;
    /** 投屏命令内容。 */
    byte[] buf;
};

/**
 * @brief STA的信息。
 *
 * @since 3.2
 * @version 1.0
 */
struct WifiStationInfo {
    /** 接收速率。 */
    unsigned int rxRate;
    /** 发送速率。 */
    unsigned int txRate;
    /** 速率传输类型。 */
    unsigned int flags;
    /** 接收VHT-MCS（Very High Throughput Modulation and Coding Scheme）配置。 */
    unsigned char rxVhtmcs;
    /** 发送VHT-MCS（Very High Throughput Modulation and Coding Scheme）配置。 */
    unsigned char txVhtmcs;
    /** 接收MCS（Modulation and Coding Scheme）索引。 */
    unsigned char rxMcs;
    /** 发送MCS（Modulation and Coding Scheme）索引。 */
    unsigned char txMcs;
    /** 接收VHT-NSS（Very High Throughput Number of Spatial Streams）配置。 */
    unsigned char rxVhtNss;
    /** 发送VHT-NSS（Very High Throughput Number of Spatial Streams）配置。 */
    unsigned char txVhtNss;
};
/** @} */
